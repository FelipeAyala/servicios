package com.curso.ws.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.curso.ws.model.Empresa;

@Repository
public interface IEmpresaDAO extends JpaRepository<Empresa, Integer>{
	@Query("from Empresa ce where lower(ce.razonSocial) like %:nombre%")
	List<Empresa> listarPorNombre(@Param("nombre") String nombre);
}
