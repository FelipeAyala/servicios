package com.curso.ws.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.curso.ws.beans.ObtenerEmpresaRequest;
import com.curso.ws.beans.ObtenerEmpresaResponse;
import com.curso.ws.service.IEmpresaService;
import com.curso.ws.util.Constants;

@Endpoint
public class EmpresaEndPoint {
	@Autowired
    private IEmpresaService configuracionService;
	
	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "obtenerEmpresaRequest")
    @ResponsePayload
    public ObtenerEmpresaResponse obtenerEmpresa(@RequestPayload ObtenerEmpresaRequest request) {
		return this.configuracionService.listarPorNombre(request);
	}
}
