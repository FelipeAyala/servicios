package com.curso.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.ws.dao.IEmpresaDAO; 
import com.curso.ws.service.IEmpresaService;
import com.curso.ws.model.Empresa;
import com.curso.ws.beans.EmpresaType;
import com.curso.ws.beans.ObtenerEmpresaRequest;
import com.curso.ws.beans.ObtenerEmpresaResponse;

@Service
public class EmpresaServiceImpl implements IEmpresaService {

	@Autowired
	private IEmpresaDAO dao;

	public Empresa registrar(Empresa t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	public Empresa modificar(Empresa t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	public void eliminar(int id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);
	}

	public Empresa listarId(int id) {
		// TODO Auto-generated method stub
		Optional<Empresa> opt = dao.findById(id);
		return opt.isPresent() ? opt.get() : new Empresa();
	}

	public List<Empresa> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	public ObtenerEmpresaResponse listarPorNombre(ObtenerEmpresaRequest request) {
		// TODO Auto-generated method stub
		ObtenerEmpresaResponse response = new ObtenerEmpresaResponse ();
		String nombre = request.getDescripcion();
		List<Empresa> lstEmpresa = dao.listarPorNombre(nombre);
		if (lstEmpresa != null && lstEmpresa.size() > 0) {		
			for (Empresa empresa : lstEmpresa) {
				EmpresaType empresaType = new EmpresaType ();
				empresaType.setDireccion(empresa.getDireccion());
				empresaType.setRazonSocial(empresa.getRazonSocial());
				empresaType.setRuc(empresa.getRuc());
				response.getEmpresa().add(empresaType);
			}
			response.setRpta(Boolean.TRUE.toString());
			response.setMensaje("Se encontraron " + lstEmpresa.size() + " coincidencias.");
		}else
		{
			response.setRpta(Boolean.FALSE.toString());
			response.setMensaje("No existen datos que mostrar.");
		}
		return response;
	}
}