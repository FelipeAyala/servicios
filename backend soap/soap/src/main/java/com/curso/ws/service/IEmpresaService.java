package com.curso.ws.service;

import com.curso.ws.model.Empresa;
import com.curso.ws.beans.ObtenerEmpresaRequest;
import com.curso.ws.beans.ObtenerEmpresaResponse;

public interface IEmpresaService extends ICRUD<Empresa>{ 
	ObtenerEmpresaResponse listarPorNombre(ObtenerEmpresaRequest request);
}