package com.curso.ws.service;
  
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO; 

public interface IEmpresaService { 
	EmpresaResponseDTO obtenerEmpresasRest(EmpresaRequestDTO request);
	EmpresaResponseDTO obtenerEmpresasSoap(EmpresaRequestDTO request);
}