package com.curso.ws.client.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;  
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO;
import com.curso.ws.util.Constants;

@Component
public class EmpresaRestClient {
	
	@Autowired
	private RestTemplate restTemplate;
	
	public EmpresaResponseDTO callObtenerEmpresa(EmpresaRequestDTO request) {
		EmpresaResponseDTO responseBody = new EmpresaResponseDTO();
        try {
        	responseBody = restTemplate.
    				postForObject(Constants.HOST_REST+"/empresa/buscar", request, EmpresaResponseDTO.class);
        	return responseBody;
        } catch (Exception e) {
            responseBody.setRpta(Boolean.FALSE.toString());
            responseBody.setMensaje("Se presentaron problemas de comunicación: " + e.getMessage());
        }
        return responseBody;
    }
}
