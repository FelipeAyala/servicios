package com.curso.ws.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmpresaResponseDTO {
	protected String rpta;
	protected String mensaje;
    protected List<EmpresaDTO> empresa;
	public String getRpta() {
		return rpta;
	}
	public void setRpta(String rpta) {
		this.rpta = rpta;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<EmpresaDTO> getEmpresa() {
		return empresa;
	}
	public void setEmpresa(List<EmpresaDTO> empresa) {
		this.empresa = empresa;
	}	 
}
