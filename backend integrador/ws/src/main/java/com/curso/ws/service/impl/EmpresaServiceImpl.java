package com.curso.ws.service.impl;

import java.util.ArrayList;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.curso.ws.dto.EmpresaDTO;
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO;
import com.curso.ws.service.IEmpresaService;
import com.curso.ws.beans.EmpresaType; 
import com.curso.ws.beans.ObtenerEmpresaResponse;
import com.curso.ws.client.rest.EmpresaRestClient;
import com.curso.ws.client.soap.EmpresaSoapClient;

@Service
public class EmpresaServiceImpl implements IEmpresaService {

	@Autowired
	private EmpresaSoapClient empresaSoapClient;
	
	@Autowired
	private EmpresaRestClient empresaRestClient; 
	
	public EmpresaResponseDTO obtenerEmpresasRest(EmpresaRequestDTO request) {
		// TODO Auto-generated method stub
		final EmpresaResponseDTO responseBody = empresaRestClient.callObtenerEmpresa(request); 
		/*
		EmpresaResponseDTO response = new ArrayList<EmpresaResponseDTO> ();
		
		for (EmpresaType dato : responseBody.getEmpresa()) {
			EmpresaResponseDTO empresa = new EmpresaResponseDTO ();
			empresa.setDireccion(dato.getDireccion());
			empresa.setRazonSocial(dato.getRazonSocial());
			empresa.setRuc(dato.getRuc());
			response.add(empresa);
		} */
		return responseBody;		
	}
 
	public EmpresaResponseDTO obtenerEmpresasSoap(EmpresaRequestDTO request) {
		// TODO Auto-generated method stub
		EmpresaResponseDTO response = new EmpresaResponseDTO ();
		ObtenerEmpresaResponse responseSoap = empresaSoapClient.callObtenerEmpresa(request.getRazonSocial());
		if(responseSoap.getRpta().equals(Boolean.TRUE.toString()))
		{	
			List<EmpresaDTO> lstEmpresa = new 	 ArrayList<EmpresaDTO> ();
			for (EmpresaType dato : responseSoap.getEmpresa()) {
				EmpresaDTO empresa = new EmpresaDTO ();
				empresa.setDireccion(dato.getDireccion());
				empresa.setRazonSocial(dato.getRazonSocial());
				empresa.setRuc(dato.getRuc());
				lstEmpresa.add(empresa);
			} 
			if (lstEmpresa.size()>0)  response.setEmpresa(lstEmpresa);
		}
		response.setMensaje(responseSoap.getMensaje());
		response.setRpta(responseSoap.getRpta());
		return response;			
	}
}