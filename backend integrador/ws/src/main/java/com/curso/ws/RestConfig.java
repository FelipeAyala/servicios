package com.curso.ws;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate template = new RestTemplate(getClientHttpRequestFactory());
		template.getMessageConverters().add(new FormHttpMessageConverter());
		template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		return template;
	}
	
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		RequestConfig config = RequestConfig
				.custom()
				.setConnectTimeout(3000)
				.setConnectionRequestTimeout(5000)
				.setSocketTimeout(5000)
				.build();
		
		CloseableHttpClient client = HttpClientBuilder
				.create()
				.setDefaultRequestConfig(config)
				.build();
		
		return new HttpComponentsClientHttpRequestFactory(client);
	}
}