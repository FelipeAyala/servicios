package com.curso.ws.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;
 
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO;
import com.curso.ws.service.IEmpresaService;
 
@RestController
@RequestMapping(value = "/empresa", produces = "application/json")
//@Api(tags = "Empresa", description = "Realiza y consulta configuracion de empresas")
public class EmpresaController {

	@Autowired
    private IEmpresaService empresaService;
	
	//@ApiOperation(value = "listarEmpresa", notes = "${empresacontroller.listarEmpresa.notes}", authorizations = {@Authorization(value = "Bearer")})
	@PostMapping(value = "/buscarRest", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listarEmpresaRest(@RequestBody EmpresaRequestDTO empresa) {
		EmpresaResponseDTO response = new EmpresaResponseDTO ();
		response = empresaService.obtenerEmpresasRest(empresa);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
	
	@PostMapping(value = "/buscarSoap", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listarEmpresaSoap(@RequestBody EmpresaRequestDTO empresa) {
		EmpresaResponseDTO response = new EmpresaResponseDTO ();
		response = empresaService.obtenerEmpresasSoap(empresa);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
}
