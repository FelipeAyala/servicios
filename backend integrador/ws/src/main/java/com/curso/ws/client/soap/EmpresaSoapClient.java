package com.curso.ws.client.soap;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Component;

import com.curso.ws.beans.EmpresaPort;
import com.curso.ws.beans.EmpresaPortService;
import com.curso.ws.beans.ObtenerEmpresaRequest;
import com.curso.ws.beans.ObtenerEmpresaResponse;
import com.curso.ws.util.Constants;

@Component
public class EmpresaSoapClient {

	public ObtenerEmpresaResponse callObtenerEmpresa(String descripcion) {
		ObtenerEmpresaResponse responseBody = new ObtenerEmpresaResponse();
        try {
        	ObtenerEmpresaRequest request=new ObtenerEmpresaRequest(); 
        	EmpresaPort empresaClient = new EmpresaPortService(new URL(Constants.URL)).getEmpresaPortSoap11();
        	request.setDescripcion(descripcion);
        	responseBody = empresaClient.obtenerEmpresa(request);
        } catch (Exception e) {
        	responseBody.setRpta(Boolean.FALSE.toString());
            responseBody.setMensaje("Se presentaron problemas de comunicación: " + e.getMessage());
        }
        return responseBody;
    }
}
