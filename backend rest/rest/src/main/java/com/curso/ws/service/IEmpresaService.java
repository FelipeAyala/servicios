package com.curso.ws.service;

import com.curso.ws.model.Empresa;
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO;

public interface IEmpresaService extends ICRUD<Empresa>{ 
	EmpresaResponseDTO listarPorNombre(EmpresaRequestDTO request);
}