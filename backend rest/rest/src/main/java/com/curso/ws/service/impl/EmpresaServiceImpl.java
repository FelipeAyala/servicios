package com.curso.ws.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.curso.ws.dao.IEmpresaDAO;
import com.curso.ws.dto.EmpresaRequestDTO;
import com.curso.ws.dto.EmpresaResponseDTO;
import com.curso.ws.model.Empresa;
import com.curso.ws.service.IEmpresaService;

@Service
public class EmpresaServiceImpl implements IEmpresaService {

	@Autowired
	private IEmpresaDAO dao;

	public Empresa registrar(Empresa t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	public Empresa modificar(Empresa t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	public void eliminar(int id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);
	}

	public Empresa listarId(int id) {
		// TODO Auto-generated method stub
		Optional<Empresa> opt = dao.findById(id);
		return opt.isPresent() ? opt.get() : new Empresa();
	}
 

	public EmpresaResponseDTO listarPorNombre(EmpresaRequestDTO request) {
		// TODO Auto-generated method stub
		EmpresaResponseDTO response = new EmpresaResponseDTO ();
		String nombre = request.getRazonSocial();
		List<Empresa> lstEmpresa = dao.listarPorNombre(nombre);
		if (lstEmpresa != null && lstEmpresa.size() > 0) {		
			response.setEmpresa(lstEmpresa);
			response.setRpta(Boolean.TRUE.toString());
			response.setMensaje("Se encontraron " + lstEmpresa.size() + " coincidencias.");
		}else
		{
			response.setRpta(Boolean.FALSE.toString());
			response.setMensaje("No existen datos que mostrar.");
		}
		return response;
	}

	@Override
	public List<Empresa> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}
}